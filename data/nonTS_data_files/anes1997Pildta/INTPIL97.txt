Version 01 Codebook
-------------------
CODEBOOK INTRODUCTION FILE
1997 PILOT STUDY
(1997.PN)















                   AMERICAN NATIONAL ELECTION STUDIES:


                           1997 PILOT STUDY



             


                               CODEBOOK










                     Center for Political Studies
                     Institute for Social Research
                      The University of Michigan










                     ICPSR ARCHIVE NUMBER 2282



                          Table of Contents


        Note: >>sections in the codebook introduction and
              codebook appendix can be navigated in the
               machine-readable files by searching ">>".



INTRODUCTORY MATERIALS   (file intpil97.cbk)
----------------------
>> 1997 GENERAL INFORMATION
>> 1997 PILOT STUDY DESCRIPTION



CODEBOOK      Note:  1996 data arre included for all 1997 Pilot respondents.
--------
   1996 VARIABLES
   1997 VARIABLES



APPENDICES   (file apppil97.cbk)
----------
>> 1997 NES Pilot Technical Note - Randomization Problem
>> 1997 CONTACT ISSUE MASTER CODE
>> 1997 MASTER CODES FOR GOVERNMENT WASTE
>> 1997 C1/C1a GROUPS ('GROUPS THAT ARE LIKE R')
>> 1996 ACCESSING GROUP-SPECIFIC DATA IN THE POST-ELECTION SURVEY
>> 1996 NATIONAL PRE/POST-ELECTION STUDY SAMPLE DESIGN
>> 1996 WEIGHTED ANALYSIS OF 1996 NES DATA
>> 1996 PROCEDURES FOR SAMPLING ERROR ESTIMATION
>> 1996 NES TECHNICAL REPORTS AND OTHER OCCASIONAL PAPERS
>> 1995 Pilot Study Reports

MASTER CODES:
>> 1996 TYPE OF RACE
>> 1996 CANDIDATE NUMBERS
>> 1996 PARTY-CANDIDATE
>> 1996 POLITICAL ADVERTISEMENTS
>> 1996 RELIGIOUS IDENTIFICATION
>> 1996 CENSUS OCCUPATION CODE (1980 CENSUS)
>> 1996 CENSUS INDUSTRY CODE (1980 CENSUS)
>> 1996 ETHNICITY/NATIONALITY 
>> 1996 STATE AND COUNTRY CODES
>> 1996 MOST IMPORTANT PROBLEMS
>> 1996 PARTY DIFFERENCES
>> 1996 CPS 2-DIGIT OCCUPATION CODES
>> 1990 CENSUS DEFINITIONS
>> Post-Stratified Cross-Sectional Analysis Weights 
   for the 1992, 1994 and 1996 NES data
>> 1996 CANDIDATE LISTS AND SAMPLE BALLOT CARDS





>> 1997 GENERAL INFORMATION 

     The American National Election studies are conducted by the 
Center for Political  Studies of the Institute for Social 
Research, under the general direction of Principal Investigator 
Steven J. Rosenstone and co-Principal Investigators Virginia Sapiro, 
Warren E. Miller, and Donald R. Kinder.  Kathryn Cirksena is the 
Director of Studies  for the National Election Study.

     The 1996 Pre- and Post-Election Study was the twenty-fourth 
in the series of studies of American national elections produced 
by the Political Behavior Program of the Survey Research Center 
and the Center for Political Studies.  Since 1978  these studies 
have been conducted under the auspices of National Science 
Foundation  Grants  (SBR-9317631, SES-9209410, SES-9009379, 
SES-8808361, SES-8341310, SES-8207580 and SOC77-08885).  The 
election studies are designed by a National Board of Overseers, 
the members of which meet several times a year to plan content 
and administration of the studies.

     A number of Pilot Studies have been conducted by the NES 
for the purpose of developing new instrumentation.  The 1997 
Pilot Study is part of this series, which also includes studies 
conducted in 1979, 1983, 1985, 1987, 1989, 1991, 1993 and 1995.  
Like the traditional time-series studies, the Pilot Studies 
are ultimately designed by the Board of Overseers, with assistance
from the scholarly community in the form of solicitations for 
study content and planning committee participation.  Like all 
of its predecessors (except the 1979 Pilot Study) the 1997 Study 
respondents are a subset of the previous year's traditional 
time-series respondents.

     Members of the NES Board of Overseers during the planning 
for the 1997 Pilot Study included Prof. David Leege (Chair), 
Notre Dame University; Prof.  Larry Bartels, Princeton 
University; Prof. Charles Franklin, University of Wisconsin at 
Madison; Prof. Wendy Rahn, University of Minnesota; Prof. 
Virginia Sapiro, University of Wisconsin at Madison; Prof. W. 
Phillips Shively, University of Minnesota; Prof. Laura Stoker, 
University of California at Berkeley; and Prof. John Zaller, 
University of California at Los Angeles.

     Virginia Sapiro chaired the 1997 Pilot Study Planning 
Committee, which included Warren Miller, as well as Charles 
Franklin, Wendy Rahn, Laura Stoker and John Zaller from the Board. 
They were joined by Frank Baumgartner (Texas A & M University), 
Janet Box-Steffensmeier (Ohio State University), Kenneth Goldstein 
(Arizona State University), Kenneth Wald (University of Florida), 
and Chris Wlezien (University of Houston). 







>> 1997 PILOT STUDY DESCRIPTION

A. Study Design
     
     The 1997 Pilot Study was conducted between September 5 and 
October 1, 1997.  The study is a reinterview of a subset of 
respondents with telephones from the 1996 Post-Election Study.  All 
fresh-cross section cases for 1996 that completed a post-election 
interview and for which telephone numbers were available were 
included in the 1997 pilot.  The balance of cases consisted of cases 
from the two previous waves, the 1994 'panel' cases and the 1992 
'panel' cases for which telephone numbers were available and a 
post-election interview was conducted in 1996.  Each of these panel 
components was represented proportionally in the initial sample
for 1997.  The initial sample consisted of 724 respondents from 
1996; 551 of these respondents completed an interview in 1997.

     The response rate is thus .76 (551/724). The number of refusals 
was 22.  The remainder of the non-interviews are persons with whom
contact was never made, or who were unavailable during the study 
perio d, for such reasons as illness or absence from home.

     The study mode was Computer Assisted Telephone Interviewing 
(CATI.)  The average interview length was 45.3 minutes.

B.  Study Content

     The content of the study reflects the NES commitment to improve 
measures of group mobilization, interest articulation and 
representation, group-based political reasoning, race and racial 
attitudes and policy, issue attitudes, human and social capital,
social choice, theories of the survey response, and other 
responses to a stimulus letter calling for ideas for content sent to 
the user community on November 11, 1996.  

     Specific topic areas in the study include:

     MOBILIZATION AND NON-ELECTORAL PARTICIPATION: 

     A battery designed to improve NES instrumentation on non-electoral 
     political participation and mobilization; specifically, respondents'
     efforts to contact public officials at different levels of 
     government during the nonnelectoral season and their reasons for 
     contact. 

     GROUP-BASED POLITICS: 

     Elaborated testing of long-standing NES instrumentation on group 
     closeness designed to evaluate both "traditional" NES instrumentation
     and investigate possible additions and improvements. 

     Group difference and group conflict as a basis of current mass 
     politics: Perceptions of paired "opposing" social groups on issue,
     ideology, party placements and vote choice. The groups include
     black and white people, Christian fundamentalists and gays and lesbians,
     and men and women. There is an embedded experiment testing the effects 
     of focusing on group difference and conflict on social trust and 
     political trust and interest.

     Group threat as a basis of group-based politics: A split-ballot 
     of items involving an experimental manipulation of the level of 
     threat in different domains and prejudices about Blacks and 
     Christian Fundamentalists.

     RESPONSE LATENCY: 

     Activated timings of response latencies on several questions to 
     extend recent NES work on certainty. 

     EVALUATIONS OF THE PRESIDENT, CONGRESS AND THE SUPREME COURT: 

     Exploration of a new battery of items to improve current 
     NES instrumentation and extend parallel measurement across 
     governmental institutions.

     RELIGION AND POLITICS: 

     Further Pilot work on the role of religion in citizens' political
     thinking; attitudes toward the role of religion and religious 
     institutions in American society and politics. 


     The use of CATI enabled a number of experimental treatments 
within the survey instrument. Random assignment to question 
wording, early-late placement and presentation order were 
applied to numerous question sequences.  Rosters of items, such 
as the thermometer and placements of groups and individuals on 
scales, were randomized in administration, to minimize order 
effects.  Indicator variables that document the use of 
split-ballot and randomization features are found in the 
codebook.

C. Data and Documentation

     Because the 551 Pilot Study respondents had also been 
interviewed in the 1996 Pre- and Post Election Studies, their 
data from those studies has been merged onto the datafile.  There 
are 551 cases in the dataset (in other words, it contains 1996 
data only for those respondents who were reinterviewed in 1997).
