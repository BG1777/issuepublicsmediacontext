# Opening CDF data (with foreign package):
library("foreign")
CDF <- read.dta("data/TS_CDF_data_files/anes_cdfdta/anes_cdf.dta")

# year is VCF0004; remove cases before 1976
CDF$year <- CDF$VCF0004
CDF <- CDF[CDF$year > 1975,]

# female -- VCF0104, Respondent Gender // 1 == male, 2 == female, 0 == NA
CDF$female <- NA
CDF$female[as.numeric(CDF$VCF0104) == 3] <- 1
CDF$female[as.numeric(CDF$VCF0104) == 2] <- 0
with(CDF, table(year, female))

# age -- VCF0101, Respondent Age // 0 == NA/missing/etc
with(CDF, table(VCF0101))
CDF$age <- CDF$VCF0101
CDF$age[CDF$age < 1] <- NA
library(plyr)
ddply(CDF, ("year"), function(df)mean(df$age, na.rm = TRUE))

# income: VCF0114
# values are irregular ranges of percentiles:
with(CDF, levels(VCF0114))
CDF$income <- with(CDF, as.numeric(VCF0114) - 1)
CDF$income[CDF$income == 0] <- NA
with(CDF, table(year, income))

# education -- VCF0140a, R Education 7-category // 8 and 9 == NA/MIA
CDF$education <- as.numeric(CDF$VCF0140a)
CDF$education[CDF$education > 7] <- NA
with(CDF, table(year, education))

# ideology -- VCF0803, R Placement: Liberal-Conservative Scale
CDF$ideology <- as.numeric(CDF$VCF0803)
CDF$ideology[CDF$ideology == 1 | CDF$ideology == 9] <- NA
CDF$ideology <- CDF$ideology -1
with(CDF, table(year, ideology))

# Democrat & Republican dummies -- VCF0302, Initial Party ID Response
CDF$Democrat <- NA
CDF$Democrat[as.numeric(CDF$VCF0302) < 5] <- 0
CDF$Democrat[as.numeric(CDF$VCF0302) == 5] <- 1
CDF$Republican <- NA
CDF$Republican[as.numeric(CDF$VCF0302) > 1 | as.numeric(CDF$VCF0302) < 6] <- 0
CDF$Republican[as.numeric(CDF$VCF0302) == 1] <- 1
with(CDF, table(year, Democrat))
with(CDF, table(year, Republican))

# general_interest -- VCF0313, R Interest in Public Affairs
CDF$general_interest <- as.numeric(CDF$VCF0313)
CDF$general_interest[CDF$general_interest == 1 | CDF$general_interest == 6] <- NA
CDF$general_interest <- CDF$general_interest -1
with(CDF, table(year, general_interest))

# general_knowledge -- VCF9036 is one Q... which party controls Senate
# VCF0729, R Know Party with House Majority Before the Election        
# VCF0730, R Know Party with House Majority After the Election
# VCF0978, Was R Response Correct: Was a House Candidate an Incumbent
CDF$Senate_correct <- NA
CDF$Senate_correct[as.numeric(CDF$VCF9036) < 3] <- 1
CDF$Senate_correct[as.numeric(CDF$VCF9036) > 3 & as.numeric(CDF$VCF9036) < 7] <- 0
CDF$HouseB4_correct <- NA
CDF$HouseB4_correct[as.numeric(CDF$VCF0729) == 2] <- 0
CDF$HouseB4_correct[as.numeric(CDF$VCF0729) == 3] <- 1
CDF$HouseAfter_correct <- NA
CDF$HouseAfter_correct[as.numeric(CDF$VCF0730) == 2] <- 0
CDF$HouseAfter_correct[as.numeric(CDF$VCF0730) == 3] <- 1
CDF$HousePol_correct <- NA
CDF$HousePol_correct[as.numeric(CDF$VCF0978) == 2] <- 0
CDF$HousePol_correct[as.numeric(CDF$VCF0978) == 1] <- 1
CDF$general_knowledge <- CDF$Senate_correct + CDF$HouseB4_correct + CDF$HouseAfter_correct +
	CDF$HousePol_correct
# reduced scale for less missing:
CDF$gen_knowledge <- CDF$HouseB4_correct + CDF$Senate_correct
with(CDF, table(year, gen_knowledge))

# political_talk -- VCF0733, How Often in the Last Week Did R Discuss Politics
CDF$political_talk <- as.numeric(CDF$VCF0733)
CDF$political_talk[CDF$political_talk == 9] <- NA
CDF$political_talk <- CDF$political_talk -1
with(CDF, table(year, political_talk))

# TV_news_national_scale -- VCF9035, How Many Days R Watched National TV News in Past Week
# usable as is, just need to drop values 8 and 9
CDF$TV_news_scale <- CDF$VCF9035
CDF$TV_news_scale[CDF$TV_news_scale > 7] <- NA
with(CDF, table(year, TV_news_scale))

# TV_news_local_scale -- NA

# newspaper_scale -- using #2 here:
#   VCF0727   How Many Articles about Election Campaigns in Newspapers 
#   VCF9033   How Many Days R Read Newspaper in Last Week                 
#   VCF9034   Does R Read Daily Newspaper 
CDF$newspaper_scale <- CDF$VCF9033
CDF$newspaper_scale[CDF$newspaper_scale > 7] <- NA
with(CDF, table(year, newspaper_scale))

# radio_scale -- VCF9032, How Many Programs about Campaigns on the Radio [2 of 2]  
CDF$radio_scale <- NA
CDF$radio_scale[as.numeric(CDF$VCF9032) == 1] <- 0
CDF$radio_scale[as.numeric(CDF$VCF9032) == 2] <- 3
CDF$radio_scale[as.numeric(CDF$VCF9032) == 3] <- 2
CDF$radio_scale[as.numeric(CDF$VCF9032) == 4] <- 1
with(CDF, table(year, radio_scale))

# web_news_scale -- #VCF0744, Does R Have Access to Internet,
# VCF0745, Has R Seen Election Campgain Information on the Internet    
# ... I'm leaving this out for now
