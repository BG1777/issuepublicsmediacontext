# variables from running "CDF_data_prep.R" in use
source("CDF_data_prep.R")

# merging media context variables:
#tax_media <- read.delim("/Users/Patron/Desktop/ANESvalise/taxes_counts.txt")
#tax_media <- read.delim("C://Users/Ben/Desktop/YMK-Mike issue publics/ANESvalise/taxes_counts.txt")

library("foreign")
library("DataCombine")
tax_media <- read.dta("data/Media Content Data/taxes_counts.dta")
tax_media <- slide(tax_media, Var = "Pro", slideBy = -1)
tax_media <- slide(tax_media, Var = "num_stories", slideBy = -1)
names(tax_media)[8:9] <- c("Pro_lagged", "num_stories_lagged")
names(tax_media)

CDF <- merge(CDF, tax_media, by = "year")

# DV: VCF0839        R Placement: Government Services/Spending Scale
with(CDF, table(year, VCF0839))
CDF$opinion_extremity_taxes <- NA
CDF$opinion_extremity_taxes[CDF$VCF0839 == 4] <- 0
CDF$opinion_extremity_taxes[CDF$VCF0839 == 3 | CDF$VCF0839 == 5] <- 1
CDF$opinion_extremity_taxes[CDF$VCF0839 == 2 | CDF$VCF0839 == 6] <- 2
CDF$opinion_extremity_taxes[CDF$VCF0839 == 1 | CDF$VCF0839 == 7] <- 3


#################################
#################################
# model to run after 4/27 meeting:
# Table 2 and Table 3: 
# drop national TV news and newspaper reading

# May 18 up to date rendition?
taxes_extremity_t2_formula <-  "opinion_extremity_taxes ~
	female + age + education + ideology + income + Democrat + Republican + 
	general_interest + gen_knowledge + TV_news_scale + newspaper_scale +
	num_stories + num_stories_lagged + Pro + Pro_lagged"
tax_extremity_t2_model <- lm(taxes_extremity_t2_formula, CDF,
	weights = VCF0009)
summary(tax_extremity_t2_model)



#################################
#################################

#################################
#################################
# supplemental analysis for taxes:
# March 2015

# original HLM
library('lme4')
HLM_interactions_formula_longrun_t <- "opinion_extremity_taxes ~ 
    female + age + education + ideology + income +
  	Democrat + Republican + general_interest + gen_knowledge +
		newspaper_scale + TV_news_scale + t_num_stories + t_num_stories_lag +
		income*t_num_stories + income*t_num_stories_lag +
		(1|year)"
HLM_interactions_model_longrun_t <- lmer(HLM_interactions_formula_longrun_t, CDF)
summary(HLM_interactions_model_longrun_t)

library(HLMdiag)
save <- varcomp.mer(HLM_interactions_model)

###

save <- RIvarcomp(HLM_interactions_model)

RIvarcomp <- function(obj) {
  components <- unlist(lapply(VarCorr(obj), diag))
  residual <- attr(VarCorr(obj), "sc")
  variance_components <- c(components, Residual = residual ^ 2)
  ICC <- components / sum(variance_components)
  list(var.components = variance_components, ICC = ICC)
}

#################################
#################################


# 1) HLM, keep all existing and add # of pro frames and lagged
tax_additions_formula <- "opinion_extremity_taxes ~ 
    female + age + education + ideology + income +
  	Democrat + Republican + general_interest + gen_knowledge +
		newspaper_scale + TV_news_scale + num_stories + num_stories_lagged +
		income*num_stories + income*num_stories_lagged +
		Pro + Pro_lagged +
		(1|year)"
tax_additions_HLM <- lmer(tax_additions_formula, CDF)

library("broom")
summary(tax_additions_HLM)

#options(digits = 2, scipen = 5)
#write.table(tidy(model2), file = "C://Users/Ben/Desktop/out.csv", sep = "\\t")

library("HLMdiag")
save <- varcomp.mer(tax_additions_HLM)

RIvarcomp(tax_additions_HLM)


HLM_null_formula <- "opinion_extremity_taxes ~ 1 + (1|year)"
HLM_null_model <- lmer(HLM_null_formula, CDF)
summary(HLM_null_model)

anova(HLM_null_model, tax_additions_HLM)



# 2) and interaction of income and pro frame as well, and income/pro-frame-lagged interaction
tax_additions_formula2 <- "opinion_extremity_taxes ~ 
    female + age + education + ideology + income +
  	Democrat + Republican + general_interest + gen_knowledge +
		newspaper_scale + TV_news_scale + num_stories + num_stories_lagged +
		income*num_stories + income*num_stories_lagged +
		Pro + Pro_lagged +
		income*Pro + income*Pro_lagged +
		(1|year)"
tax_additions_HLM2 <- lmer(tax_additions_formula2, CDF)
summary(tax_additions_HLM2)
RIvarcomp(tax_additions_HLM2)

HLM_null_formula <- "opinion_extremity_taxes ~ 1 + (1|year)"
HLM_null_model <- lmer(HLM_null_formula, CDF)
summary(HLM_null_model)

anova(HLM_null_model, tax_additions_HLM2)



# 3) Next, same but remove interactions involving # of stories and # of stories lagged
tax_additions_formula3 <- "opinion_extremity_taxes ~ 
    female + age + education + ideology + income +
  	Democrat + Republican + general_interest + gen_knowledge +
		newspaper_scale + TV_news_scale + num_stories + num_stories_lagged +
#		income*num_stories + income*num_stories_lagged +
		Pro + Pro_lagged +
		income*Pro + income*Pro_lagged +
		(1|year)"
tax_additions_HLM3 <- lmer(tax_additions_formula3, CDF)
summary(tax_additions_HLM3)

RIvarcomp(tax_additions_HLM3)

HLM_null_formula <- "opinion_extremity_taxes ~ 1 + (1|year)"
HLM_null_model <- lmer(HLM_null_formula, CDF)
summary(HLM_null_model)

anova(HLM_null_model, tax_additions_HLM3)



#################################
#################################


# Table 2 --> for taxes, add two variables, # pro frames and # of pro frames lagged

# original model:
LM_formula_longrun_t <- "opinion_extremity_taxes ~
	female + age + education + ideology + income + Democrat + Republican + 
	general_interest + gen_knowledge + TV_news_scale + newspaper_scale +
	num_stories + num_stories_lagged"
LM_model_longrun_t <- lm(LM_formula_longrun_t, CDF,
	weights = VCF0009)
summary(LM_model_longrun_t)

# adding those two variables
tax_additions_LM_formula <-  "opinion_extremity_taxes ~
	female + age + education + ideology + income + Democrat + Republican + 
	general_interest + gen_knowledge + TV_news_scale + newspaper_scale +
	num_stories + num_stories_lagged + Pro + Pro_lagged"
tax_additions_LM_model <- lm(tax_additions_LM_formula, CDF,
	weights = VCF0009)
summary(tax_additions_LM_model)




tidy()
tidy($coefficients)

options(digits = 2, scipen = 7)
write.table(tidy(), file = "C://Users/Ben/Desktop/out.csv", sep = "\\t")
