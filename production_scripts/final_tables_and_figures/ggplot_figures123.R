library("foreign"); library("dplyr"); library("reshape"); library("ggplot2")

##################
## load media data --> reshape for multiple lines with legend (melt) --> graph

## abortion media
abortion_media <- read.dta("data/Media Content Data/abort_counts.dta")
abortion_media <- select(abortion_media, year, num_stories, Pro, Con)
names(abortion_media) <- c("year", "total stories", "pro stories", "con stories")
abortion_media <- melt(abortion_media, id.vars = "year")

ggplot(abortion_media, aes(x = year, y = value, linetype = variable)) + geom_line() +
    scale_linetype_manual("variable", values = c("solid", "dotted", "longdash")) +
    labs(title = "Table 1: Pro, con, and total abortion stories by year",
        x = "year", y = "number of stories") + theme_bw() +
    theme(legend.title = element_blank())


## immigration media
immigration_media <- read.csv("data/Media Content Data/Immigration_counts.csv")
names(immigration_media)[2] <- "num_stories"
immigration_media <- select(immigration_media, year, num_stories, Pro, Con)
names(immigration_media) <- c("year", "total stories", "pro stories", "con stories")
immigration_media <- melt(immigration_media, id.vars = "year")

ggplot(immigration_media, aes(x = year, y = value, linetype = variable)) + geom_line() +
    scale_linetype_manual("variable", values = c("solid", "dotted", "longdash")) +
    labs(title = "Table 2: Pro, con, and total immigration stories by year",
        x = "year", y = "number of stories") + theme_bw() +
    theme(legend.title = element_blank())


## taxes media
tax_media <- read.dta("data/Media Content Data/taxes_counts.dta")
tax_media <- select(tax_media, year, num_stories, Pro, Con)
names(tax_media) <- c("year", "total stories", "pro stories", "con stories")
tax_media <- melt(tax_media, id.vars = "year")

ggplot(tax_media, aes(x = year, y = value, linetype = variable)) + geom_line() +
    scale_linetype_manual("variable", values = c("solid", "dotted", "longdash")) +
    labs(title = "Table 3: Pro, con, and total tax stories by year",
        x = "year", y = "number of stories") + theme_bw() +
    theme(legend.title = element_blank())


######################################################







    theme(legend.title = element_blank(),
        legend.background = element_rect(fill = "white"),
        plot.background = element_rect(fill = "white"),
        panel.background = element_rect(fill = "white")),
        plot.background = element_rect(fill = "white"),
        plot.background = element_rect(fill = "white"),)
    
    