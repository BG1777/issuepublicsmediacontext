Title: Collinearity of media content variables in abortion HLM from table 3


______________________________________________________
Contents: 

--> List of media contgent variables in full HLM
--> Correlation matrix of media content variables
--> F-test of full HLM vs. no pro or pro_lagged variables
--> F-test of full HLM vs. no num_stories or num_stories_lagged
--> VIFs?
--> HLM results of model without pro or pro_lagged terms
--> HLM results of model without num_stories or num_stories_lagged terms


______________________________________________________
List of media content variables in full HLM:

--> num_stories 
--> num_stories_lagged
--> num_stories_lagged*religiosity
--> num_stories*religiosity
--> Pro 
--> Pro_lagged


______________________________________________________
Correlation matrix of media content variables

> cor(CDF[,c("num_stories", "num_stories_lagged", "Pro", "Pro_lagged")],
+     use = "complete.obs")

                   num_stories num_stories_lagged       Pro Pro_lagged
num_stories          1.0000000          0.3638764 0.8685757  0.1976434
num_stories_lagged   0.3638764          1.0000000 0.3419031  0.8697943
Pro                  0.8685757          0.3419031 1.0000000  0.2628193
Pro_lagged           0.1976434          0.8697943 0.2628193  1.0000000


______________________________________________________
F-test of full HLM vs. no pro or pro_lagged variables

> anova(model_3.1_abortion_no_pro_media, model_3.1_abortion)

       Df   AIC   BIC  logLik deviance  Chisq Chi Df Pr(>Chisq)  
object 17 12860 12981 -6412.8    12826                           
..1    19 12859 12994 -6410.3    12821 4.9359      2    0.08476 .

Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1


______________________________________________________
F-test of full HLM vs. no num_stories, num_stories_lagged, or *religiosity interactions

> anova(model_3.1_abortion_no_num_stories, model_3.1_abortion)

       Df   AIC   BIC  logLik deviance  Chisq Chi Df Pr(>Chisq)
object 15 12853 12960 -6411.4    12823                         
..1    19 12859 12994 -6410.3    12821 2.2149      4     0.6963



______________________________________________________
VIF info

NA


______________________________________________________
HLM without pro or pro_lagged variables

> summary(model_3.1_abortion_no_pro_media)

Random effects:
 Groups   Name        Variance  Std.Dev.
 year     (Intercept) 0.0002918 0.01708 
 Residual             0.2291572 0.47870 
Number of obs: 9405, groups:  year, 10

Fixed effects:
                                 Estimate Std. Error         df t value Pr(>|t|)    
(Intercept)                     5.892e-01  4.691e-02  7.400e+01  12.561  < 2e-16 ***
female                          8.253e-02  1.042e-02  9.390e+03   7.918 2.66e-15 ***
age                            -9.741e-04  3.187e-04  9.161e+03  -3.056  0.00225 ** 
education                       2.401e-02  3.456e-03  8.397e+03   6.947 4.00e-12 ***
ideology                       -3.818e-02  4.095e-03  9.386e+03  -9.325  < 2e-16 ***
income                          6.600e-03  4.920e-03  9.314e+03   1.341  0.17987    
religiosity                    -7.025e-02  1.474e-02  9.387e+03  -4.766 1.90e-06 ***
Democrat                        3.652e-02  1.223e-02  9.390e+03   2.986  0.00283 ** 
Republican                     -2.731e-02  1.287e-02  9.389e+03  -2.122  0.03386 *  
general_interest                8.123e-03  6.042e-03  8.921e+03   1.344  0.17888    
gen_knowledge                   1.962e-03  6.933e-03  4.822e+03   0.283  0.77717    
num_stories                     2.354e-05  1.539e-04  2.700e+01   0.153  0.87954    
num_stories_lagged              4.196e-04  1.393e-04  2.300e+01   3.012  0.00622 ** 
religiosity:num_stories_lagged -7.062e-05  5.544e-05  9.386e+03  -1.274  0.20276    
religiosity:num_stories         3.205e-05  6.276e-05  9.388e+03   0.511  0.60966    
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

______________________________________________________
HLM without num_stories or num_stories_lagged or *religiosity interactions

> summary(model_3.1_abortion_no_num_stories)

Random effects:
 Groups   Name        Variance  Std.Dev.
 year     (Intercept) 8.602e-05 0.009275
 Residual             2.292e-01 0.478707
Number of obs: 9405, groups:  year, 10

Fixed effects:
                   Estimate Std. Error         df t value Pr(>|t|)    
(Intercept)       5.937e-01  3.631e-02  8.500e+01  16.352  < 2e-16 ***
female            8.224e-02  1.042e-02  9.392e+03   7.891 3.33e-15 ***
age              -9.535e-04  3.182e-04  8.803e+03  -2.997  0.00274 ** 
education         2.444e-02  3.445e-03  7.363e+03   7.094 1.42e-12 ***
ideology         -3.838e-02  4.089e-03  9.324e+03  -9.385  < 2e-16 ***
income            6.390e-03  4.914e-03  9.088e+03   1.300  0.19353    
religiosity      -7.763e-02  4.454e-03  9.392e+03 -17.428  < 2e-16 ***
Democrat          3.607e-02  1.222e-02  9.375e+03   2.952  0.00317 ** 
Republican       -2.715e-02  1.287e-02  9.391e+03  -2.110  0.03486 *  
general_interest  8.468e-03  6.029e-03  8.051e+03   1.405  0.16020    
gen_knowledge     1.419e-03  6.899e-03  2.969e+03   0.206  0.83699    
Pro               6.320e-04  3.305e-04  7.000e+00   1.913  0.09970 .  
Pro_lagged        1.100e-03  2.871e-04  5.000e+00   3.830  0.01038 *  
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1



